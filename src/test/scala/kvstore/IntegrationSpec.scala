/**
 * Copyright (C) 2013-2015 Typesafe Inc. <http://www.typesafe.com>
 */
package kvstore

import akka.actor.{ Actor, Props, ActorRef, ActorSystem }
import akka.testkit.{ TestProbe, ImplicitSender, TestKit }
import org.scalatest._
import scala.concurrent.duration._
import org.scalactic.ConversionCheckedTripleEquals

class IntegrationSpec(_system: ActorSystem) extends TestKit(_system)
with FunSuiteLike
with Matchers
with BeforeAndAfterAll
with ConversionCheckedTripleEquals
with ImplicitSender
with Tools {

  import Replica._
  import Replicator._
  import Arbiter._

  def this() = this(ActorSystem("ReplicatorSpec"))

  override def afterAll: Unit = system.shutdown()

  /*
   * Recommendation: write a test case that verifies proper function of the whole system,
   * then run that with flaky Persistence and/or unreliable communication (injected by
   * using an Arbiter variant that introduces randomly message-dropping forwarder Actors).
   */
  test("Primary and secondaries must work in concert when persistence is unreliable") {
    val arbiter = TestProbe()//system.actorOf(Arbiter.props(), "case1-arbiter")
    val primary = system.actorOf(Replica.props(arbiter.ref, Persistence.props(flaky = true)), "case1-primary")

    arbiter.expectMsg(Join)
    arbiter.send(primary, JoinedPrimary)

    val secondary = system.actorOf(Replica.props(arbiter.ref, Persistence.props(flaky = true)), "case1-secondary")

    arbiter.expectMsg(Join)
    arbiter.send(secondary, JoinedSecondary)
    arbiter.send(primary, Replicas(Set(primary, secondary)))

    val user1 = session(primary)
    val user2 = session(secondary)

    // Synchronous
    user1.setAcked("msg", "Hello")
    user1.get("msg") shouldBe Some("Hello")
    user2.get("msg") shouldBe Some("Hello")
    user1.setAcked("data", "Secret data")
    user2.get("data") shouldBe Some("Secret data")
    user1.setAcked("answer", "42")
    user1.get("answer") shouldBe Some("42")

    // Asynchronous
    user1.set("msg", "Abrakadabra")
    user2.get("msg") should contain oneOf("Hello", "Abrakadabra")

    // Change secondaries
    val newSecondaries = (1 to 2).map { i =>
      val actor = system.actorOf(Replica.props(arbiter.ref, Persistence.props(flaky = true)))
      arbiter.send(actor, JoinedSecondary)
      actor
    }
    val users = newSecondaries.map(session)

    arbiter.send(primary, Replicas(newSecondaries.toSet + primary))

    users foreach { user =>
      user.getAndVerify("msg")
    }
  }
}
