package kvstore

import akka.actor.SupervisorStrategy.{Restart, Resume}
import akka.actor._
import kvstore.Arbiter._
import kvstore.Persistence.{Persisted, Persist, PersistenceException}
import scala.concurrent.duration._
import scala.util.Random

object Replica {

  sealed trait Operation {
    def key: String

    def id: Long
  }

  case class Insert(key: String, value: String, id: Long) extends Operation

  case class Remove(key: String, id: Long) extends Operation

  case class Get(key: String, id: Long) extends Operation

  case class PersistRetry(key: String, maybeString: Option[String], id: Long) extends Operation

  case class PersistFailed(key: String, id: Long, seq: Long) extends Operation

  sealed trait OperationReply

  case class OperationAck(id: Long) extends OperationReply

  case class OperationFailed(id: Long) extends OperationReply

  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  case class RequestInfo(requester: ActorRef, var acknowledgers: Set[ActorRef], var persisted: Boolean = false)

  case class Value(var value: Option[String],
                   var acknowledgers: Set[ActorRef] = Set.empty[ActorRef],
                   var seq: Long = 0,
                   var persisted: Boolean = false)

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor with ActorLogging {

  import Replica._
  import Replicator._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  var kv = Map.empty[String, Value].withDefaultValue(Value(None))
  //var keySeq = Map.empty[String, Long]//.withDefaultValue(0L)

  val persistence = context.actorOf(persistenceProps)
  val persistRetryInterval = 150 milliseconds
  val persistTimeout = 1 second

  // the current set of replicators
  var replicators = Set.empty[ActorRef]
  // replicator of secondary replica
  var replicatorOption: Option[ActorRef] = None
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]

  // map from request id to request objects
  var requests = Map.empty[Long, RequestInfo]

  override val supervisorStrategy: SupervisorStrategy = OneForOneStrategy() {
    case _: PersistenceException =>
      log.debug("There was an PersistenceException")
      Resume
    case any =>
      log.debug("There was another exception")
      Restart
  }

  private[this] def generateId = Random.nextLong()

  arbiter ! Join

  def receive = {
    case JoinedPrimary => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  private[this] val leaderBehaviour: Receive = {
    case Replicas(replicas) => updateReplicas(replicas)
    case Insert(key, value, id) => saveKey(key, Some(value), id)
    case Remove(key, id) => saveKey(key, None, id)
    case Persisted(key, id) =>
      kv(key).seq += 1L
      kv(key).persisted = true
      checkOperationFinished(id, persisted = true)
    case Replicated(key, id) =>
      checkOperationFinished(id)
    case PersistFailed(key, id, seq) =>
      if (requests.get(id).isDefined && !(requests(id).acknowledgers.isEmpty && requests(id).persisted)) {
        requests(id).requester ! OperationFailed(id)
        requests -= id
      }
  }

  private[this] val replicaBehaviour: Receive = {
    case Snapshot(key, value, seq) =>
      replicatorOption = Some(sender())
      snapshot(key, value, seq)
    case Persisted(key, id) =>
      kv(key).persisted = true
      replicatorOption.foreach(_ ! SnapshotAck(key, kv(key).seq))
      kv(key).seq += 1L
  }

  private[this] val commonBehaviour: Receive = {
    case Get(key, id) =>
      readKey(key, id)
    case PersistRetry(key, maybeString, id) =>
      if (!kv(key).persisted) {
        persist(key, maybeString, id)
      }
  }

  private[this] def updateReplicas(repls: Set[ActorRef]) = {
    val newReplicas = repls.filter(_ != self)
    val currentReplicas = secondaries.keySet

    val freshReplicas = newReplicas.filterNot(currentReplicas.contains)
    freshReplicas foreach { replica =>
      val replicator = context.actorOf(Props(new Replicator(replica)))
      secondaries += replica -> replicator
      replicators += replicator
      kv foreach { pair =>
        val requestId = generateId
        val request = RequestInfo(self, Set.empty[ActorRef], persisted = true)
        requests += requestId -> request
        replicator ! Replicate(pair._1, pair._2.value, requestId)
      }
    }

    val oldReplicas = currentReplicas.filterNot(newReplicas.contains)
    oldReplicas foreach { replica =>
      val replicator = secondaries(replica)
      replicators -= replicator
      secondaries -= replica
      requests foreach { pair =>
        val requestId = pair._1
        val request = pair._2
        request.acknowledgers -= replicator
        if (request.acknowledgers.isEmpty) {
          request.requester ! OperationAck(requestId)
        }
      }
      context.stop(replicator)
    }
  }

  private[this] def saveKey(key: String, valueOption: Option[String], id: Long) = {
    kv += key -> Value(valueOption, replicators, kv(key).seq)
    persist(key, valueOption, id)
    replicateKey(key, valueOption, id)
    context.system.scheduler.scheduleOnce(persistTimeout, self, PersistFailed(key, id, kv(key).seq))
  }

  private[this] def replicateKey(key: String, valueOption: Option[String], id: Long) = {
    val requester = sender()
    val request = RequestInfo(requester, replicators)
    requests += (id -> request)
    replicators foreach (_ ! Replicate(key, valueOption, id))
  }

  private[this] def snapshot(key: String, maybeString: Option[String], seq: Long) = {
    val expectedSeq = kv(key).seq
    if (seq <= expectedSeq) {
      if (seq == expectedSeq) {
        kv += key -> Value(maybeString, Set.empty[ActorRef], seq)
        persist(key, maybeString, generateId)
      } else {
        sender() ! SnapshotAck(key, seq)
      }
    }
  }

  private[this] def persist(key: String, maybeString: Option[String], id: Long) = {
    persistence ! Persist(key, maybeString, id)
    context.system.scheduler.scheduleOnce(persistRetryInterval, self, PersistRetry(key, maybeString, id))
  }

  private[this] def checkOperationFinished(id: Long, persisted: Boolean = false) = {
    if (requests.get(id).isDefined) {
      if (persisted) {
        requests(id).persisted = true
      } else {
        val acknowledger = sender()
        requests(id).acknowledgers -= acknowledger
      }
      if (requests(id).acknowledgers.isEmpty && requests(id).persisted) {
        requests(id).requester ! OperationAck(id)
        requests -= id
      }
    }
  }

  private[this] def readKey(key: String, id: Long) = {
    sender() ! GetResult(key, kv(key).value, id)
  }

  val leader: Receive = leaderBehaviour orElse commonBehaviour

  val replica: Receive = replicaBehaviour orElse commonBehaviour
}

