package kvstore

import akka.actor.{Actor, ActorRef, Props}

import scala.concurrent.duration._

object Replicator {

  case class Replicate(key: String, valueOption: Option[String], id: Long)

  case class ReplicateRetry(key: String, valueOption: Option[String], seq: Long)

  case class Replicated(key: String, id: Long)

  case class Snapshot(key: String, valueOption: Option[String], seq: Long)

  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor {

  import Replicator._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from (key, sequence) number to pair of sender and request
  var acks = Map.empty[(String, Long), (ActorRef, Replicate)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]

  //var keySeq = Map.empty[String, Long].withDefaultValue(0L)

  var _seqCounter = 0L

  val replicateRetryInterval = 200 milliseconds

  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  def receive: Receive = {
    case msg@Replicate(key, valueOption, id) =>
      val seq = nextSeq
      //keySeq += key -> (seq + 1)
      acks += (key, seq) -> (sender(), msg)
      replicate(key, valueOption, seq)
    case ReplicateRetry(key, valueOption, seq) => replicate(key, valueOption, seq)
    case SnapshotAck(key, seq) =>
      val (requester, msg) = acks(key -> seq)
      acks -= key -> seq
      requester ! Replicated(msg.key, msg.id)
  }

  private[this] def replicate(key: String, valueOption: Option[String], seq: Long): Unit = {
    if (acks.get(key -> seq).isDefined) {
      replica ! Snapshot(key, valueOption, seq)
      context.system.scheduler.scheduleOnce(replicateRetryInterval, self, ReplicateRetry(key, valueOption, seq))
    }
  }
}
